import boto3


def get_secret_value(secret_key: str) -> str:
    client = boto3.client(
        service_name="secretsmanager", region_name="eu-west-1"
    )
    response = client.get_secret_value(SecretId=secret_key)
    return response["SecretString"]
