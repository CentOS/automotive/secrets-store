import boto3
import pytest
from moto import mock_secretsmanager

from secrets_store import main


@pytest.mark.parametrize("key,value", [("secret_key", "secret_value")])
@mock_secretsmanager
def test_get_secret_value(key: str, value: str) -> bool:
    client = boto3.client(
        service_name="secretsmanager", region_name="eu-west-1"
    )
    client.create_secret(
        Name=key,
        SecretString=value,
    )
    assert main.get_secret_value(secret_key=key) == value
